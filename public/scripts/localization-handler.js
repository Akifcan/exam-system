


async function setAutoLang() {
    if (localStorage.lang) {
        setLang(localStorage.lang)
    } else {
        localStorage.lang = 'tr'
        setLang(localStorage.lang)
        setTimeout(() => {
            setLang(localStorage.lang)
        }, 800)
    }
}

async function setLang(langCode) {

    const months = [
        "january",
        "february",
        "march",
        "april",
        "may",
        "june",
        "july",
        "august",
        "september",
        "october",
        "november",
        "december"
    ]
    const date = new Date()


    const response = await fetch(`/lang/${langCode}.json`)
    const data = await response.json()
    if (document.getElementById('month')) {
        document.getElementById('month').textContent = data[months[date.getMonth()]]
    }

    Object.keys(data).forEach(key => {
        if (document.querySelector(`[data-translate="${key}"]`)) {
            document.querySelectorAll(`[data-translate="${key}"]`).forEach(item => {
                if (item) {
                    item.textContent = data[key]
                }
            })
        }
    })
}

setAutoLang()
