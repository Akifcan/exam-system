import express from 'express';
import exphbs from 'express-handlebars';
import expressHandlebarsSections from 'express-handlebars-sections';
import fileupload from 'express-fileupload';
import http from 'http';
import { Server } from 'socket.io';
import questions from './public/data/questions.json';
import amqp from 'amqplib/callback_api.js';
import path from 'path';
const __dirname = path.resolve();

const app = express();
const server = http.createServer(app);
const io = new Server(server);

app.use(fileupload());
app.use(express.json());
app.use('/scripts', express.static(__dirname + '/node_modules'));
app.use(express.urlencoded({ extended: true }));
app.use(express.static('public'));

const PORT = 3001;

app.engine(
  'handlebars',
  exphbs({
    helpers: {
      section: expressHandlebarsSections(),
      increase: function (value) {
        return (value += 1);
      },
    },
  })
);

app.set('view engine', 'handlebars');

app.get('/', (req, res) => {
  res.render('home');
});

app.get('/exam/:id', (req, res) => {
  const question = questions.find((q) => q.examId == req.params.id);
  if (!question) res.redirect('/');
  res.render('exam', { question });
});

app.get('/login', (req, res) => {
  res.render('login', { layout: 'auth' });
});

app.get('/teacher', (req, res) => res.render('teacher'));

app.post('/upload', (req, res) => {
  console.log(req.files);
  if (req.files['exam-video']) {
    const examOfStudent = req.files['exam-video'];
    const mockStudentNumber = Math.floor(Math.random() * 1000000);
    examOfStudent.mv(
      `public/uploads/${examOfStudent.name}-${mockStudentNumber}.mp4`
    );
    res.status(200).json({ message: 'OK' });
  } else {
    res.status(400).json({ message: 'NO FILE' });
  }
});

server.listen(PORT, () => {
  console.log(`Up on ${PORT}`);
});

amqp.connect('amqp://localhost', function (error0, connection) {
  if (error0) {
    throw error0;
  }
  app.post('/send-message/:faculty', (req, res) => {
    const faculty = req.params.faculty;
    console.log(faculty);
    connection.createChannel(function (error1, channel) {
      if (error1) {
        throw error1;
      }
      channel.assertQueue(faculty, {
        durable: false,
      });
      channel.sendToQueue(faculty, Buffer.from(JSON.stringify(req.body)));
    });
    res.send(true);
  });

  io.on('connection', (socket) => {
    socket.on('join-faculty', (faculty) => {
      socket.join(faculty);
      connection.createChannel(function (error1, channel) {
        if (error1) {
          throw error1;
        }
        channel.assertQueue(faculty, {
          durable: false,
        });
        channel.consume(
          faculty,
          function (msg) {
            io.to(faculty).emit('student-action-info', msg.content.toString());
          },
          { noAck: false }
        );
      });

      console.log('will join' + faculty);
    });
  });
});
